package everythinginmultithreading;

public class Assignment2 {
	public static void main(String[] args) {
		Thread enna = new Thread (()-> {
			threadStartFunnction();
		});
		Thread minna = new Thread (()-> {
			threadStartFunnction();
		});
		Thread dicca = new Thread (()-> {
			threadStartFunnction();
		});
		enna.start();
		minna.start();
		dicca.start();
		System.out.println("I have started Thread");
	}

	private static void threadStartFunnction() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Local :" 	+i);
		}
		
	}
}
