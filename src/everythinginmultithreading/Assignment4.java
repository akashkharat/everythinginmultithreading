package everythinginmultithreading;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class MyThreadLock implements Runnable {

	Lock lock = new ReentrantLock();

	// This method will print local value and Will throw exception for "Dicca"
	@Override
	public void run() {
		try {
			lock.lock();
			for (int i = 0; i < 10; i++) {
				System.out.println("Local :" + Thread.currentThread().getName() + i);
				if(Thread.currentThread().getName().equals("dicca") && i ==5) {
					throw new Exception("Dicca Thak gayi");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			lock.unlock();
		}

	}
}

public class Assignment4 {
	public static void main(String[] args) {
		MyThreadLock myThreadLock = new MyThreadLock();
		
		Thread enna = new Thread(myThreadLock);
		enna.setName("enna");
		
		Thread minna = new Thread(myThreadLock);
		minna.setName("minna");
		
		Thread dicca = new Thread(myThreadLock);
		dicca.setName("dicca");
		
		enna.start();
		minna.start();
		dicca.start();
		System.out.println("I have started Thread");
	}
}
