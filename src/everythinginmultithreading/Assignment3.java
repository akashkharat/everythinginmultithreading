package everythinginmultithreading;

class MyThread implements Runnable {

	// This method will print local value and Will throw exception for "Dicca"
	@Override
	public synchronized void run() {
		try {
			for (int local = 0; local < 10; local++) {
				System.out.println("Local :" + Thread.currentThread().getName() + local);
				if(Thread.currentThread().getName().equals("dicca") && local ==5) {
					throw new Exception("Dicca Thak gayi");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}

public class Assignment3 {
	public static void main(String[] args) {
		MyThread myThread = new MyThread();
		
		Thread enna = new Thread(myThread);
		enna.setName("enna");
		
		Thread minna = new Thread(myThread);
		minna.setName("minna");
		
		Thread dicca = new Thread(myThread);
		dicca.setName("dicca");
		
		enna.start();
		minna.start();
		dicca.start();
		
		System.out.println("I have started Thread");
	}
}
